package colors;

import java.awt.Color;

public class JSortingLineColors
{
	private Color sorted;
	private Color unSorted;
	private Color primaryCompare;
	private Color secondaryCompare;
	private Color primarySwap;
	private Color secondarySwap;
	
	public JSortingLineColors()
	{
		this.sorted = Color.DARK_GRAY;
		this.unSorted = Color.LIGHT_GRAY;
		this.primaryCompare = Color.RED;
		this.secondaryCompare = Color.ORANGE;
		this.primarySwap = Color.YELLOW;
		this.secondarySwap = Color.YELLOW;
	}
	
	public Color getSorted()
	{
		return this.sorted;
	}
	
	public Color getUnSorted()
	{
		return this.unSorted;
	}
	
	public Color getPrimaryCompare()
	{
		return this.primaryCompare;
	}
	
	public Color getSecondaryCompare()
	{
		return this.secondaryCompare;
	}
	
	public Color getPrimarySwap()
	{
		return this.primarySwap;
	}
	
	public Color getSecondarySwap()
	{
		return this.secondarySwap;
	}
	
	public JSortingLineColors setSorted(Color c)
	{
		this.sorted = c;
		return this;
	}
	
	public JSortingLineColors setUnSorted(Color c)
	{
		this.unSorted = c;
		return this;
	}
	
	public JSortingLineColors setPrimaryCompare(Color c)
	{
		this.primaryCompare = c;
		return this;
	}
	
	public JSortingLineColors setSecondaryCompare(Color c)
	{
		this.secondaryCompare = c;
		return this;
	}
	
	public JSortingLineColors setPrimarySwap(Color c)
	{
		this.primarySwap = c;
		return this;
	}
	
	public JSortingLineColors setSecondarySwap(Color c)
	{
		this.secondarySwap = c;
		return this;
	}
	
	public JSortingLineColors setCompare(Color primary, Color secondary)
	{
		this.primaryCompare = primary;
		this.secondaryCompare = secondary;
		return this;
	}
	
	public JSortingLineColors setSwap(Color primary, Color secondary)
	{
		this.primarySwap = primary;
		this.secondarySwap = secondary;
		return this;
	}
	
}
