package util;

import java.util.Random;

public class Bit
{
	private Bit()
	{ // this is a static utility class. No instances.
	}

	private static char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', 'A', 'B', 'C', 'D', 'E', 'F', };

	/**
	 * Includes preceding 0s
	 * 
	 * @param num
	 * @return
	 */
	public static String toFullBinaryString(int num)
	{
		char[] buf = new char[32];
		for(int i = 31; i >= 0; --i)
		{
			buf[i] = digits[num & 0x1];
			num >>>= 1;
		}
		return String.valueOf(buf);
	}

	/**
	 * Includes preceding 0s
	 * 
	 * @param num
	 * @return
	 */
	public static String toFullBinaryString(long num)
	{
		char[] buf = new char[64];
		for(int i = 63; i >= 0; --i)
		{
			buf[i] = digits[(int) (num & 0x1)];
			num >>>= 1;
		}
		return String.valueOf(buf);
	}

	public static String toFullHexString(int num)
	{
		char[] buf = new char[8];
		for(int i = 7; i >= 0; --i)
		{
			buf[i] = digits[num & 0xF];
			num >>>= 4;
		}
		return String.valueOf(buf);
	}

	public static String toFullHexString(long num)
	{
		char[] buf = new char[16];
		for(int i = 15; i >= 0; --i)
		{
			buf[i] = digits[(int) num & 0xF];
			num >>>= 4;
		}
		return String.valueOf(buf);
	}

	/**
	 * Unsigned log base 2
	 * 
	 * @param n
	 * @return
	 */
	public static int log2u(long n)
	{
		if(n == 0L)
			throw new IllegalArgumentException("cannot take log2 of 0");

		return 63 - Long.numberOfLeadingZeros(n);
	}

	private static final Random rand = new Random();

	public static int randInt(int min, int max)
	{
		return rand.nextInt(max - min) + min;
	}

	public static int popCount(long n)
	{
		n -= (n >> 1) & 0x5555555555555555L;
		n = (n & 0x3333333333333333L) + ((n >> 2) & 0x3333333333333333L);
		n = (n + (n >> 4)) & 0x0F0F0F0F0F0F0F0FL;
		n += n >> 8;
		n += n >> 16;
		n += n >> 32;
		return (int) n & 0x7F;
	}
}
