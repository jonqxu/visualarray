package util;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Console
{
	private static Scanner sysin = new Scanner(System.in);

	private Console()
	{ /* No instantiation, utility class */
	}

	public static String nextLine()
	{
		return sysin.nextLine();
	}

	@SafeVarargs
	public static <T extends Enum<T>> T getNextEnum(Class<T> clazz,
			String errorMessage, T... illegal)
	{
		T result = null;
		boolean failedInput = true;
		do
		{
			try
			{
				result = nextEnum(clazz, illegal);
				failedInput = false;
			}
			catch(InvalidInputException e)
			{
				if(errorMessage != null)
					System.out.print(formatError(errorMessage, e.getInput()));
			}
		}
		while(failedInput);
		return result;
	}

	@SafeVarargs
	public static <T extends Enum<T>> T nextEnum(Class<T> clazz, T... illegal)
	{
		String input = nextLine().toUpperCase();
		T result = null;
		try
		{
			result = Enum.valueOf(clazz, input);
		}
		catch(IllegalArgumentException e)
		{
			throw InvalidInputException.fromInput(input);
		}

		if(illegal != null)
			for(T t : illegal)
				if(t == result)
					throw InvalidInputException.fromInput(input);

		return result;
	}

	public static int getNextInt(int min, int max, int radix,
			String errorMessage)
	{
		boolean failedInput = true;
		int result = 0;
		do
		{
			try
			{
				result = nextInt(min, max, radix);
				failedInput = false;
			}
			catch(InvalidInputException e)
			{
				if(errorMessage != null)
					System.out.print(formatError(errorMessage, e.getInput()));
			}
		}
		while(failedInput);
		return result;
	}

	public static int nextInt(int min, int max, int radix)
	{
		if(max < min)
			throw new IllegalArgumentException();
		if(radix < Character.MIN_RADIX)
			throw new NumberFormatException("radix " + radix
					+ " less than Character.MIN_RADIX");
		if(radix > Character.MAX_RADIX)
			throw new NumberFormatException("radix " + radix
					+ " greater than Character.MAX_RADIX");

		String input = nextLine();
		int length = input.length();
		if(length == 0)
			throw new InvalidInputException("Empty input");

		int result = 0;
		if(input.charAt(0) == '-')
		{
			if(length == 1)
				throw InvalidInputException.fromInput(input);
			int multmin = min / radix;
			for(int i = 1; i < length; ++i)
			{
				if(result < multmin)
					throw InvalidInputException.fromInput(input);

				int digit = Character.digit(input.charAt(i), radix);
				if(digit < 0)
					throw InvalidInputException.fromInput(input);
				result *= radix;
				if(result < min + digit)
					throw InvalidInputException.fromInput(input);
				result -= digit;
			}
			if(result > max)
				throw InvalidInputException.fromInput(input);
		}
		else
		{
			int multmin = max / radix;
			for(int i = 0; i < length; ++i)
			{
				if(result > multmin)
					throw InvalidInputException.fromInput(input);

				int digit = Character.digit(input.charAt(i), radix);
				if(digit < 0)
					throw InvalidInputException.fromInput(input);
				result *= radix;
				if(result > max - digit)
					throw InvalidInputException.fromInput(input);
				result += digit;
			}
			if(result < min)
				throw InvalidInputException.fromInput(input);
		}

		return result;
	}

	public static String getNextRegex(String regex, String errorMessage)
	{
		Pattern p = Pattern.compile(regex);
		String input = nextLine();
		while(!p.matcher(input).matches())
		{
			System.out.print(formatError(errorMessage, input));
			input = nextLine();
		}

		return input;
	}

	private static String formatError(String errorMessage, Object input)
	{
		return errorMessage.replace("%input", '"' + input.toString() + '"');
	}

	public static String defaultError = "Invalid input: %input\n";
}
