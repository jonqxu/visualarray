package util;

public class InvalidInputException extends RuntimeException
{
	private static final long serialVersionUID = 8065065865441873332L;

	private Object input;

	public InvalidInputException(Object input)
	{
		super();
		this.input = input;
	}

	public InvalidInputException(Object input, String message)
	{
		super(message);
		this.input = input;
	}

	public static InvalidInputException fromInput(Object input)
	{
		return new InvalidInputException(input, "Invalid input \"" + input
				+ "\"");
	}

	public Object getInput()
	{
		return input;
	}
}
