package util;

public class StringMethodUtils
{
	private StringMethodUtils()
	{
	}
	
	public static String toMethodName(String input)
	{
		if(input == null || input.equals(""))
			return input;
		
		String noSpace  = input.replaceAll("\\s", "");
		
		
		int end = noSpace.indexOf('(');
		if(end < 0)
		{
			char[] buf = noSpace.toCharArray();
			buf[0] = Character.toLowerCase(buf[0]);
			return String.valueOf(buf);
		}
		
		char[] buf = noSpace.substring(0, end).toCharArray();
		buf[0] = Character.toLowerCase(buf[0]);
		return String.valueOf(buf);
	}
	
	public static Object[] getNumberArgs(String input)
	{
		final int start = input.indexOf('(');
		final int end = input.indexOf(')');
		
		if(start < 0 || start == end - 1)
		{
			return new Number[0];
		}
		
		String argsString = input.substring(start + 1, end);
		
		String[] argsArray = argsString.split(", *");
		
		final int numOfArgs = argsArray.length;
		Object[] args = new Object[numOfArgs];
		
		for(int i = 0; i < numOfArgs; ++i)
		{
			String cur = argsArray[i];
			
			if(cur.indexOf('.') < 0)
			{
				args[i] = Integer.parseInt(cur);
			}
			else
			{
				args[i] = Double.parseDouble(cur);
			}
		}
		
		return args;
	}
	
	public static Class<?>[] numberClassUnBox(Object[] args)
	{
		if(args == null)
			return null;
		
		int len = args.length;
		
		Class<?>[] buf = new Class<?>[len];
		for(int i = 0; i <len; ++i)
		{
			Object cur = args[i];
			if(cur.getClass() == Integer.class)
			{
				buf[i] = int.class;
			}
			else if(cur.getClass() == Double.class)
			{
				buf[i] = double.class;
			}
		}
		return buf;
	}
}
