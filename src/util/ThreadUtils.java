package util;

public class ThreadUtils
{
	private ThreadUtils(){}
	
	public static void sleepNanos(long nanos) throws InterruptedException
	{
		if(nanos < 0)
			throw new IllegalArgumentException("Negative timeout " + nanos);
		
		if(nanos == 0)
			return;
		
		long end = System.nanoTime() + nanos;
		
		Thread.sleep(nanos / 1000000);
		
		while(System.nanoTime() < end)
		{
			if(Thread.interrupted())
				throw new InterruptedException("sleep interrupted");
		}
	}
}
