package util;

import java.util.Random;

public class Rand
{
	private static Random rand = new Random();

	/**
	 * Random Integer Returns a random integer from [0, n).
	 * 
	 * @param n
	 *            the maximum exclusive
	 * @return the random integer
	 */
	public static int randInt(int n)
	{
		return rand.nextInt(n);
	}

	/**
	 * Random Integer Returns a random integer from [min, max)
	 * 
	 * @param min
	 * @param max
	 * @return the random integer
	 */
	public static int randInt(int min, int max)
	{
		return rand.nextInt(max - min) + min;
	}
}
