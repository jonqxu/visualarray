package visual.gui;

import java.awt.Toolkit;

public final class Vars
{
	private Vars()
	{ // static variables only
	}

	final static int maxWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
	final static int maxHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
}
