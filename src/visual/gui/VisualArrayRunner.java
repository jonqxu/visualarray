package visual.gui;

import java.awt.Dialog;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JDialog;

import visual.sort.VisualArraySorter;

public class VisualArrayRunner extends Thread
{
	private VisualArray va;
	private Dialog vaHolder;
	private VisualArraySorter sorter;
	private Set<ActionListener> disposeListeners = new HashSet<>();

	private final WindowAdapter EXIT_ACTION = new WindowAdapter()
	{
		@Override
		public void windowClosing(WindowEvent e)
		{
			VisualArrayRunner.this.dispose(0);
		}
	};

	public VisualArrayRunner(VisualArray va)
	{
		super();
		this.va = va;
	}

	public void init(Window owner, VisualArraySorter sorter, Point location)
	{
		this.sorter = sorter;
		
		vaHolder = new JDialog(owner, this.sorter.toString());
		
		vaHolder.addWindowListener(EXIT_ACTION);
		vaHolder.add(va);

		vaHolder.pack();
		vaHolder.setLocation(location);
		vaHolder.setResizable(false);
		vaHolder.setFocusableWindowState(false);
		vaHolder.setVisible(true);
	}

	@Override
	public void run()
	{
		if(sorter == null)
			throw new IllegalStateException("not initialized");
		
		try
		{
			va.runSorter(sorter);
		}
		catch(InterruptedException e)
		{
			dispose(0);
		}
	}
	
	public void dispose(int modifiers)
	{
		if(vaHolder != null)
		{
			ActionEvent disposeEvent = new ActionEvent(this, 0, null, System.currentTimeMillis(), modifiers);
			
			
			for(ActionListener l : disposeListeners)
				l.actionPerformed(disposeEvent);
			
			vaHolder.setVisible(false);
			vaHolder.dispose();
			va = null;
			vaHolder = null;
			this.interrupt();
		}
	}
	
	public void addDisposeListener(ActionListener listener)
	{
		disposeListeners.add(listener);
	}
	
	public Window getWindow()
	{
		return vaHolder;
	}
}
