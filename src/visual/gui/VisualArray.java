package visual.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.util.Arrays;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import visual.sort.VisualArraySorter;

public class VisualArray extends JPanel
{
	private final int length;
	private final int thickness;
	private final int padding;
	private final int[] initialValues;
	private JSortingLine[] elements;
	
	public void runSorter(VisualArraySorter sorter) throws InterruptedException
	{
		sorter.sort(elements);
	}
	
	public void reset()
	{
		for(int i = 0; i < length; ++i)
		{
			JSortingLine cur = elements[i];
			cur.markSorted(false);
			cur.setLength(initialValues[i]);
		}
	}

	public VisualArray(int[] x, final int thickness, final int padding)
	{
		super();
		
		this.length = x.length;
		this.thickness = thickness;
		this.padding = padding;
		
		if(x == null || length < 1)
			throw new IllegalArgumentException("Illegal array");
		
		this.initialValues = Arrays.copyOf(x, length);
		
		this.elements = new JSortingLine[length];
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		elements[0] = new JSortingLine(x[0], thickness);
		this.add(elements[0]);
		for(int i = 1; i < length; ++i)
		{
			this.add(Box.createVerticalStrut(padding));
			elements[i] = new JSortingLine(x[i], thickness);
			this.add(elements[i]);
		}

		int min = 0;
		int max = 0;
		for(int cur : x)
		{
			if(cur > max)
				max = cur;
			if(cur < min)
				min = cur;
		}

		Dimension size = new Dimension(max - min + 9, length
				* (padding + thickness) - padding + 14);

		this.setBorder(new LeftColoredBorder(8, 1, 8, 8, Color.DARK_GRAY));
		this.setMinimumSize(size);
		this.setPreferredSize(size);
		this.setSize(size);
	}
	
	public VisualArray copy()
	{
		return new VisualArray(initialValues, thickness, padding);
	}

	private static final long serialVersionUID = -8756728352668944947L;
}
