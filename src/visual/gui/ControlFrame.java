package visual.gui;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import visual.build.ArrayCondition;
import visual.sort.VisualArraySorter;

public class ControlFrame extends JFrame
{
	private JPanel buttonsPanel = new JPanel();

	private final InitVisButton[] buttons;
	private final JButton initAll = new JButton("init all")
	{
		{
			addActionListener(new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					setEnabled(false);
					for(InitVisButton b : buttons)
					{
						b.doClick(0);
					}
				}
			});
		}
		private static final long serialVersionUID = -765069814336525169L;
	};
	
	private Set<VisualArrayRunner> varSet = new HashSet<>(VisualArraySorter.size);
	private final JButton startAll = new JButton("start")
	{
		{
			setEnabled(false);
			addActionListener(new ActionListener()
			{
				private volatile boolean reset = true;
				
				@Override
				public void actionPerformed(ActionEvent e)
				{
					if(reset)
					{
						for(InitVisButton b : buttons)
						{
							b.setEnabled(false);
						}
						initAll.setEnabled(false);

						setText("clear");

						for(VisualArrayRunner vis : varSet)
						{
							vis.start();
						}
						
						reset = false;
					}
					else
					{
						setEnabled(false);
						setText("start");
						
						for(VisualArrayRunner var : varSet)
						{
							var.dispose(1);
						}
						varSet.clear();
						
						for(InitVisButton b : buttons)
						{
							b.setEnabled(true);
						}
						initAll.setEnabled(true);
						
						reset = true;
						
						
					}
				}
			});
		}
		
		private static final long serialVersionUID = 5891062758282201054L;
	};
	private final VisualArray initial;

	public ControlFrame(final ArrayCondition condition, final int size, final int thickness, final int padding)
	{
		super("Visual Array Control");
		
		if(size < 0)
			throw new IllegalArgumentException("Illegal size " + size);
		
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.PAGE_AXIS));
		buttonsPanel.setBorder(BorderFactory.createEmptyBorder(16, 32, 8, 32));

		JPanel initButtonsPanel = new JPanel();
		initButtonsPanel.setAlignmentX(JPanel.CENTER_ALIGNMENT);
		initButtonsPanel.setMinimumSize(new Dimension(160, 50));
		initButtonsPanel.setLayout(new BoxLayout(initButtonsPanel, BoxLayout.PAGE_AXIS));

		VisualArraySorter[] algorithms = VisualArraySorter.values();
		this.initial = new VisualArray(condition.build(size * (thickness + padding), size), thickness, padding);
		
		int len = algorithms.length;
		buttons = new InitVisButton[len];
		for(int i = 0; i < len; ++i)
		{
			InitVisButton button = new InitVisButton(algorithms[i]);
			buttons[i] = button;
			initButtonsPanel.add(button);
		}
		
		initButtonsPanel.add(Box.createVerticalStrut(10));
		initButtonsPanel.add(initAll);
		
		buttonsPanel.add(initButtonsPanel);
		buttonsPanel.add(Box.createVerticalStrut(20));
		buttonsPanel.add(startAll);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(buttonsPanel);

		pack();
		Dimension frameSize = getSize();
		setLocation(Vars.maxWidth - frameSize.width - 40, Vars.maxHeight - frameSize.height - 40);
		setResizable(false);
		
		JDialog test = new JDialog(this);
		test.add(initial);
		test.pack();
		points = new LocationGrid(test.getSize());
	}
	
	private final LocationGrid points;
	private ActionListener removeVar = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Object source = e.getSource();
			if(!(source instanceof VisualArrayRunner))
				throw new IllegalStateException("removeLocation invoked on invalid source " + source);
			
			if(e.getModifiers() != 1)
				varSet.remove(source);
			Window container = ((VisualArrayRunner)source).getWindow();
			points.remove(container.getLocation());
		}
	};
	
	
	private class InitVisButton extends JButton
	{
		private VisualArraySorter sorter;

		public InitVisButton(VisualArraySorter sorter)
		{
			super("init: " + sorter.toString());
			this.sorter = sorter;
			this.setAlignmentX(CENTER_ALIGNMENT);
			this.setFocusable(false);
			this.addActionListener(initVisAction);
		}

		private final ActionListener initVisAction = new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				InitVisButton.this.setEnabled(false);
				VisualArrayRunner var = new VisualArrayRunner(initial.copy());
				var.addDisposeListener(removeVar);
				var.init(ControlFrame.this, sorter, points.getNext());
				varSet.add(var);
				startAll.setEnabled(true);
			}
		};

		private static final long serialVersionUID = -343450407226334403L;
	}

	private static final long serialVersionUID = 230130258759536245L;
}
