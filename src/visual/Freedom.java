package visual;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import visual.build.ArrayCondition;
import visual.gui.ControlFrame;
import visual.gui.JSortingLine;

public class Freedom
{
	static final int SIZE = 100;
	static final int THICKNESS = 2;
	static final int PADDING = 1;
	static final ArrayCondition initial = ArrayCondition.UNIQUELY_RANDOM;
	
	public static void main(String[] args)
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JSortingLine.setCompareDelay(20);
		JSortingLine.setSwapDelay(0.0);

		ControlFrame mainFrame = new ControlFrame(initial, SIZE, THICKNESS, PADDING);
		mainFrame.setVisible(true);

		new Thread()
		{
			@Override
			public void run()
			{
				try
				{
					Thread.sleep(600000);
				}
				catch(InterruptedException e)
				{
					Thread.currentThread().interrupt();
				}
				finally
				{
					System.exit(0);
				}
			}
		}.start();
	}
}
