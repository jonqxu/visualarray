package visual.sort;

import org.apache.commons.lang3.text.WordUtils;

import util.Rand;
import visual.gui.JSortingLine;

public enum VisualArraySorter
{
	BUBBLE_SORT
	{
		@Override
		public void sort(JSortingLine[] elements) throws InterruptedException
		{
			int i = elements.length;
			
			while(i > 0)
			{
				int changeIndex = 0;
				for(int j = 1; j < i; ++j)
				{
					JSortingLine fir = elements[j - 1];
					JSortingLine sec = elements[j];
					if(fir.compareTo(sec) > 0)
					{
						fir.swap(sec);
						changeIndex = j;
					}
				}
				while(i > changeIndex)
					elements[--i].markSorted(true);
			}
		}
	},
	
	QUICK_SORT
	{
		@Override
		public void sort(JSortingLine[] elements) throws InterruptedException
		{
			quickSort(elements, 0, elements.length - 1);
		}

		private void quickSort(JSortingLine[] elements, int lower, int upper)
				throws InterruptedException
		{
			int difference = upper - lower;

			if(difference < 0)
				return;

			if(difference == 0)
			{
				elements[upper].markSorted(true);
				return;
			}

			int pivot = Rand.randInt(lower, upper + 1);
			elements[pivot].swap(elements[lower]);
			pivot = lower;
			int direction = 1;

			int i = upper;
			while(i - pivot != 0)
			{
				JSortingLine piv = elements[pivot];
				JSortingLine cur = elements[i];

				if((piv.compareTo(cur) > 0) == (direction > 0))
				{
					piv.swap(cur);
					int tmp = pivot;
					pivot = i;
					i = tmp;
					direction = -direction;
				}

				i -= direction;
			}

			elements[pivot].markSorted(true);

			quickSort(elements, lower, pivot - 1);
			quickSort(elements, pivot + 1, upper);
		}
	},
	
	INSERTION_SORT
	{
		@Override
		public void sort(JSortingLine[] elements) throws InterruptedException
		{
			elements[0].markSorted(true);
			for(int i = 1; i < elements.length; ++i)
			{
				int icur = i;
				JSortingLine fir;
				JSortingLine sec;
				while(icur >= 1
						&& (sec = elements[icur])
								.compareTo(fir = elements[icur - 1]) < 0)
				{
					fir.swap(sec);
					--icur;
				}
				elements[icur].markSorted(true);
			}
		}
	},
	
	SELECTION_SORT
	{
		@Override
		public void sort(JSortingLine[] elements) throws InterruptedException
		{
			for(int i = elements.length - 1; i >= 0; --i)
			{
				JSortingLine last = elements[i];
				JSortingLine max = last;
				for(int j = i - 1; j >= 0; --j)
				{
					JSortingLine cur = elements[j];
					if(max.compareTo(cur) < 0)
						max = cur;
				}
				last.swap(max);
				last.markSorted(true);
			}
		}
	},
	
	COMB_SORT
	{
		@Override
		public void sort(JSortingLine[] elements) throws InterruptedException
		{
			combInsertionSort(elements, 1.35, 1);
		}
		
		private void combInsertionSort(JSortingLine[] elements, final double shrinkFactor, final int bound) throws InterruptedException
		{
			if(shrinkFactor < 0.0)
				throw new IllegalArgumentException("Illegal shrink factor "
						+ shrinkFactor);
			if(bound < 1)
				throw new IllegalArgumentException("Illegal bound " + bound);

			int gap = elements.length;

			while(gap > bound)
			{
				gap = (int) (gap / shrinkFactor);// 1.247330950103979
				if(gap < 1)
					gap = 1;

				int i = 0;
				boolean changed = false;
				while(i + gap < elements.length)
				{
					final JSortingLine fir = elements[i];
					final JSortingLine sec = elements[i + gap];
					if(fir.compareTo(sec) > 0)
					{
						fir.swap(sec);
						changed = true;
					}
					++i;
				}

				if(!changed)
					gap = (int) (gap / shrinkFactor);
			}
			INSERTION_SORT.sort(elements);
		}
	},
	
	HEAP_SORT
	{
		@Override
		public void sort(JSortingLine[] elements) throws InterruptedException
		{
			int start = (elements.length - 2) / 2;
			while(start >= 0)
			{
				heapSink(elements, start, elements.length - 1);
				--start;
			}

			int end = elements.length - 1;
			while(end > 0)
			{
				elements[0].swap(elements[end]);
				elements[end].markSorted(true);
				--end;
				heapSink(elements, 0, end);
			}
			elements[0].markSorted(true);
		}

		private void heapSink(JSortingLine[] elements, int start, int end)
				throws InterruptedException
		{
			int root = start;

			while(root * 2 < end)
			{
				int child = root * 2 + 1;
				int swap = root;
				if(elements[swap].compareTo(elements[child]) < 0)
				{
					swap = child;
				}
				if(child < end
						&& elements[swap].compareTo(elements[child + 1]) < 0)
				{
					swap = child + 1;
				}
				if(swap != root)
				{
					elements[root].swap(elements[swap]);
					root = swap;
				}
				else
					return;
			}
		}
	},
	
	COCKTAIL_SORT
	{
		@Override
		public void sort(JSortingLine[] elements) throws InterruptedException
		{
			int first = 0;
			int last = elements.length - 1;
			while(first < last)
			{
				int shift = first;
				for(int j = first; j < last; ++j)
				{
					JSortingLine fir = elements[j];
					JSortingLine sec = elements[j + 1];
					if(fir.compareTo(sec) > 0)
					{
						fir.swap(sec);
						shift = j;
					}
				}
				while(last > shift)
					elements[last--].markSorted(true);

				shift = last;
				for(int j = last; j >= first; --j)
				{
					JSortingLine fir = elements[j];
					JSortingLine sec = elements[j + 1];
					if(fir.compareTo(sec) > 0)
					{
						fir.swap(sec);
						shift = j;
					}
				}
				while(first <= shift)
					elements[first++].markSorted(true);
			}
		}
	},
	
	SHELL_SORT
	{
		@Override
		public void sort(JSortingLine[] elements) throws InterruptedException
		{
			int len = elements.length;
			int gap = 1;
			while(gap < len - 1)
				gap = 3 * gap + 1;

			while(gap > 1)
			{
				for(int i = 0; i < gap; ++i)
				{
					for(int j = i + gap; j < len; j += gap)
					{
						int jcur = j;
						JSortingLine fir;
						JSortingLine sec;
						while(jcur >= gap
								&& (sec = elements[jcur])
										.compareTo(fir = elements[jcur - gap]) < 0)
						{
							fir.swap(sec);
							jcur -= gap;
						}
					}
				}

				gap /= 3;
			}
			INSERTION_SORT.sort(elements);
		}
	};
	
	public static final int size = values().length;
	
	private final String name;
	
	private VisualArraySorter()
	{
		this.name = WordUtils.capitalize(name().replace('_', ' ').toLowerCase());
	}
	
	private VisualArraySorter(String name)
	{
		this.name = name;
	}
	
	public String toString()
	{
		return name;
	}
	
	public abstract void sort(JSortingLine[] elements)
			throws InterruptedException;
}
